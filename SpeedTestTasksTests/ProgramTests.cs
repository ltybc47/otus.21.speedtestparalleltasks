﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;

namespace SpeedTestTasks.Tests
{
  [TestClass()]
  public class ProgramTests
  {

    [TestMethod()]
    public void TimeTest_SynchroneTest()
    {
      Program.Count = 100_000;
      Program.TimeTest(SynchroneTest);

      Program.Count = 1_000_000;
      Program.TimeTest(SynchroneTest);

      Program.Count = 10_000_000;
      Program.TimeTest(SynchroneTest);

      void SynchroneTest(int[] arr)
      {
        var sum = arr.Sum(x => x);
        Console.WriteLine(sum);
      }
    }

    [TestMethod()]
    public void TimeTest_ASynchroneTest_10_000_000()
    {
      Program.Count = 10_000_000;
      Program.TimeTest(AsinchroneTest);
    }

    public void AsinchroneTest(int[] arr)
    {
      int sum = 0;
      int countThread = 4;
      List<Thread> listThread = new List<Thread>();
      int[] results = new int[countThread+1];

      for (int i = 0; i < countThread; i++)
      {
        listThread.Add(new Thread(() =>
        {
          int localSum = 0;
          for (int j = 0 + arr.Length / countThread * (i-1); j < arr.Length / countThread * i; j++)
          {
            localSum += arr[j];
          }
          results[i] = localSum;
        }        
          ));
        //listThread.Last().Start();
      }

      foreach (Thread thread in listThread)
      {
        thread.Start();
      }
      foreach (Thread thread in listThread)
      {
        thread.Join();
      }
      //listThread.ForEach(x => { x.Start(); x.Join(); });
      //listThread.ForEach(x => x.Join());
      Console.WriteLine(results.Sum(x => x));
    }

    [TestMethod()]
    public void TimeTest_ASynchroneTest_1_000_000()
    {
      Program.Count = 1_000_000;
      Program.TimeTest(AsinchroneTest);
    }

    [TestMethod()]
    public void TimeTest_ASynchroneTest_100_000()
    {
      Program.Count = 100_000;
      Program.TimeTest(AsinchroneTest);
    }

    [TestMethod()]
    public void TimeTest_LinqASynchroneTest()
    {

      Program.Count = 100_000;
      Program.TimeTest(AsinchroneLinq);

      Program.Count = 1_000_000;
      Program.TimeTest(AsinchroneLinq);

      Program.Count = 10_000_000;
      Program.TimeTest(AsinchroneLinq);

      void AsinchroneLinq(int[] arr)
      {
        int sum = 0;
        //arr.AsParallel().Select(item => sum += item).ToList();
        sum = arr.AsParallel().Sum();
        Console.WriteLine(sum);
      }
    }
  }
}