﻿using System;
using System.Diagnostics;
using System.Linq;

namespace SpeedTestTasks
{
  public class Program
  {

    static void Main(string[] args)
    {
      TimeTest((int[] x) => { Console.WriteLine("test"); });
    }

    public static int Count = 1_000_0000;
    private static int[] Array;

    public static void TimeTest(Action<int[]> act)
    {
      Array = GenerateArray(Count);
      Show(Array);
      var timer = Stopwatch.StartNew();
      act(Array);
      timer.Stop();
      Console.WriteLine($"Время: {timer.ElapsedMilliseconds} мс.");
      Show(Array);
    }

    private static Random Random = new Random();

    private static int[] GenerateArray(int count)
    {
      var arr = new int[count];
      for (var i = 0; i < count; i++)
        arr[i] = Random.Next(10);
      return arr;
    }

    private static void Show(int[] arr, int n = 15)
    {
      Console.WriteLine(string.Join(", ", arr.Take(n)));
    }
  }
}
